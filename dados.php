<?php 
session_start();

if (isset($_POST) && empty($_POST)) {
	exit();	
} else{

	require_once 'securit/security.csrf.php';

	$security = new \security\CSRF;
	
	if(isset($_POST['token'])) {
		
		if($security->get($_POST['token'])) {
			
			try{
				$security->delete($_POST['token']);
				
				$conexao = new PDO("mysql:host=localhost;dbname=ione_black_friday;charset=utf8","root","");
			} catch(PDOException  $e ){
				die("Error: ".$e->getMessage());
			}

			try {
				
				$whatsapp = $_POST['whatsapp'];
				$email = $_POST['email'];
				
				$stmt = $conexao->prepare("INSERT INTO cadastros_lp (whatsapp, email) VALUES (:whatsapp, :email)");
				$stmt->bindParam(':whatsapp', $whatsapp);
				$stmt->bindParam(':email', $email);
				$stmt->execute();
			} catch (Exception $e) {
				die("Error: ".$e->getMessage());	
			}
		} else {

			exit();

		}
	}
}
?>