<?php 
error_reporting(E_ERROR | E_PARSE);
session_start();

require_once 'securit/security.csrf.php';

$security = new \security\CSRF;
$token = $security->set(3, 3600);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>IONE</title>

    <!-- <link rel="icon" type="image/png" href="assets/imgs/icons/favicon.png"/>
    <link rel="icon" type="image/png" href="assets/imgs/icons/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="assets/imgs/icons/favicon-16x16.png" sizes="16x16" /> -->

</head>
<body>
<header>
    <figure>
        <img src="assets/images/logo.png" class="d-block mx-auto img-fluid">
    </figure>
    <h1 class="text-center san-light fs-25 text-uppercase text-white mb-1">
        Vem aí a melhor <b class="san-bold">Black Friday</b>
    </h1>
    <figure class="figure-title">
        <img src="assets/images/title.png" class="d-block mx-auto img-fluid">
    </figure>
</header>
<section class="countdown pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <p id="demo" class="san-bold fs-30 text-white"></p>
                <div class="d-flex conjunto-count" align="center">
                    <div class="dias">
                        <img src="assets/images/2.png" class="direito " height="400">
                        <img src="assets/images/0.png" class="esquerda " height="400">
                    </div>
                    <div class="pontos-ini">
                        <img src="assets/images/dois-pontos.png" class="" height="400">
                    </div>
                    <div class="horas">
                        <img src="assets/images/3.png" class="direito " height="400">
                        <img src="assets/images/4.png" class="esquerda " height="400">
                    </div>
                    <div class="pontos">
                        <img src="assets/images/dois-pontos.png" class="" height="400">
                    </div>
                    <div class="minutos">
                        <img src="assets/images/0.png" class="direito " height="400">
                        <img src="assets/images/8.png" class="esquerda " height="400">
                    </div>
                </div>
                <p id="teste" class="san-bold fs-30 text-white"></p>
            </div>
        </div>
    </div>
</section>
<section class="section-form">
    <div class="container">
        <div class="row">
            <h2 class="fs-30 text-center san-regular text-white max-650 mx-auto">
                Cadastre seu e-mail e fique por dentro <span class="azul">das ofertas do Black Weekend antes de todo mundo.</span>
            </h2>
            <div class="col-md-12" align="center">
                <form name="form_inscricao" class="formulario needs-validation" action="dados.php" method="post" novalidate>
                    <div class="form-group">
                        <input type="text" name="whatsapp" class="form-control san-regular input-form" placeholder="Digite seu whatsapp">
                    </div>
                    <div class="input-group" align="center">
                        <input type="text" name="email" class="input-medio form-control san-regular" placeholder="Digite seu e-mail">
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                        <input type="button" name="enviar" class="button-form" value="Cadastrar" onclick="javascript: return validar();">
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="mt-5 pt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="caixa-item">
                    <figure>
                        <img src="assets/images/facebook.png" class="d-block mx-auto">
                    </figure>
                    <div class="caixa-text cinza fs-20">
                        <h3 class="san-regular fs-24 azul">
                            /iOneStoreBr
                        </h3>
                        Seguir
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="caixa-item">
                    <figure>
                        <img src="assets/images/twitter.png" class="d-block mx-auto">
                    </figure>
                    <div class="caixa-text cinza fs-20">
                        <h3 class="san-regular fs-24 azul">
                            /iOneStoreBr
                        </h3>
                        Seguir
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="caixa-item b-none">
                    <figure>
                        <img src="assets/images/instagram.png" class="d-block mx-auto">
                    </figure>
                    <div class="caixa-text cinza fs-20">
                        <h3 class="san-regular fs-24 azul">
                            /iOneStoreBr
                        </h3>
                        Seguir
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="san-light text-white fs-18 text-center mt-5 pt-5 mb-5">
                    iOne - Tecno Indústria e Comércio de Computadores LTDA - atendimento@ionestore.com.br<br/>
                    CNPJ: 07.272.825/0004-57 - Rua Cleia, 440 - Barroso - Fortaleza - Ceará - CEP: 60863-280
                </p>
            </div>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="assets/js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
</body>
</html>